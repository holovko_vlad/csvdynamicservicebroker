﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;
using HCB = SourceCode.Hosting.Client.BaseAPI;
using SOC = SourceCode.SmartObjects.Client;
using System.Xml.Linq;
using Microsoft.VisualBasic.FileIO;
using System.Data.SqlClient;


namespace CSVDynamicServiceBroker
{

    public class CSVHelper
    {
        #region Class Fields
        
        private string sqlConnectionString = string.Empty;
        private string csvTemplateFilePath = string.Empty;
        private string databaseName = string.Empty;
        private string tableName = string.Empty;
        
        private ServiceConfiguration serviceConfiguration;

        #endregion

        #region Constructor
        public CSVHelper(ServiceConfiguration configuration)
        {

            try
            {
                serviceConfiguration = configuration;

                sqlConnectionString = configuration["SQL Connection String"].ToString();
                csvTemplateFilePath = configuration["templateFilePath"].ToString();

                databaseName = GetDatabaseNameFromSqlConnectionString(sqlConnectionString);
                tableName = GetFileNameFromFilePath(csvTemplateFilePath);
            }
            catch (Exception ex)
            {
                throw new Exception("\nCannot get configuration:\n" + ex.Message);
            }

        }
        #endregion

        #region Generate
        public void Generate()
        {
            
            try
            {
                GenerateDBTableFromCSVTemplateFile(databaseName, tableName, csvTemplateFilePath);
            }
            catch (Exception ex)
            {
                throw new Exception("\nMessage: Generate Database Table from CSV TemplateFile\n" + ex.Message);
            }

        }
        #endregion

        #region Helper Methods

        private void GenerateDBTableFromCSVTemplateFile(string databaseName, string tableName, string csvTemplateFilePath)
        {
            if (csvTemplateFilePath.Length < 5 ||
                csvTemplateFilePath.Substring(csvTemplateFilePath.Length - 3).ToLower() != "csv")
            {
                throw new Exception("CSV Template File [" + csvTemplateFilePath + "] has wrong extension. It must be .csv");
            }
            if (!File.Exists(csvTemplateFilePath))
            {
                throw new Exception("CSV Template File [" + csvTemplateFilePath + "] does not exist");
            }

            using (TextFieldParser parser = new TextFieldParser(@csvTemplateFilePath))
            {
                string[] columns = { };
                string[] dataTypes = { };

                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                if (!parser.EndOfData)
                {
                    columns = parser.ReadFields();
                }

                if (!parser.EndOfData)
                {
                    dataTypes = parser.ReadFields();
                }
                
                if (columns.Count() == dataTypes.Count())
                {
                    List<string> existingColumns = getExistingColumns(databaseName,tableName);
                    
                    existingColumns.RemoveRange(0,2);
                    int countList = existingColumns.Count();

                    if (countList != columns.Count() || isDifferentColumns(existingColumns,columns))
                    {
                        DropTable(databaseName, tableName);
                        CreateTable(databaseName, tableName, columns, dataTypes);
                    }
                    
                }
                else
                {
                    throw new Exception("\nMessage: Count of Columns and DataTypes in template file must be equal\n");
                }

            }

        }

        private bool isDifferentColumns(List<string> existingColumns, string[] columns){

            foreach(var c in existingColumns){
                if (!Array.Exists(columns, element => element == c))
                {
                    return true;
                }
            }
            return false;
        }

        private List<string> getExistingColumns(string databaseName, string tableName){

            string query = "SELECT COLUMN_NAME " +
                            "FROM " + databaseName + ".INFORMATION_SCHEMA.COLUMNS " +
                            "WHERE TABLE_NAME = N'" + tableName + "'";

            List<string> list = new List<string>();

            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                SqlCommand myCommand = new SqlCommand(query, conn);
                try
                {
                    var result = myCommand.ExecuteReader(); 
                    while (result.Read())
                    {
                        list.Add((string)result.GetValue(0));
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("\nMessage: Can not execute" +
                                        "\nException Message: " + ex.Message);
                }
            }
            return list;
        }

        private void DropTable(string databaseName, string tableName)
        {
            string query = "USE [" + databaseName + "] IF OBJECT_ID('dbo." + tableName + "', 'U') IS NOT NULL DROP TABLE dbo." + tableName + "";

            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                SqlCommand myCommand = new SqlCommand(query, conn);
                try
                {
                    myCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("\nMessage: Can not drop table" +
                                        "\nException Message: " + ex.Message);
                }
            }
        }

        public void CreateSequence()
        {
            string query = "IF NOT EXISTS (SELECT name FROM sys.sequences WHERE name = N'csvServiceBrokerSequence') " +
            "BEGIN " +
                "USE [" + databaseName + "] " +
                "CREATE SEQUENCE [dbo].[csvServiceBrokerSequence] " +
                "AS [int] " +
                "START WITH 1 " +
                "INCREMENT BY 1 " +
                "MINVALUE -2147483648 " +
                "MAXVALUE 2147483647 " +
                "CACHE " +
            "END";

            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                SqlCommand myCommand = new SqlCommand(query, conn);
                try
                {
                    myCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("\nMessage: Can not create SQL Sequence" +
                                        "\nException Message: " + ex.Message);
                }
            }
        }

        private void CreateTable(string databaseName, string tableName, string[] columns, string[] dataTypes)
        {
            string cols = "";
            for (int i = 0; i < columns.Count(); i++)
            {
                cols += "[" + columns[i] + "] " + dataTypes[i] + " NULL,\n";
            }

            string query =
                "USE [" + databaseName + "] SET ANSI_NULLS ON SET QUOTED_IDENTIFIER ON SET ANSI_PADDING ON " +
                "CREATE TABLE [dbo].[" + tableName + "]( " +
                "[ID] [bigint] IDENTITY(1,1) NOT NULL," +
                "[BatchID] [int] NOT NULL,\n" +
                cols +
                "CONSTRAINT [PK_" + tableName + "] PRIMARY KEY CLUSTERED ([ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] SET ANSI_PADDING OFF " +
                "";

            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();
                SqlCommand myCommand = new SqlCommand(query, conn);
                try
                {
                    myCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception("\nMessage: Can not create table" +
                                        "\nException Message: " + ex.Message);
                }
            }
        
        }

        private string GetInsertQuery(string tableName, int BatchId, string[] fields)
        {
            string values = "" + BatchId + "";
            foreach(var field in fields) {
                values += ",'" + field + "'";
            }

            string query = "INSERT INTO [dbo].[" + tableName + "] VALUES (" + values + ")";
            return query;
        }

        public string[] InsertCSVRows(string xmlString)
        {
            string[] resultMessage = { "" , "" };
            string errorMessage = "";
            
            int BatchId = GetBatchId();
            if (BatchId == 0)
            {
                throw new Exception("\nMessage: BatchId = 0\n");
            }

            TextFieldParser parser = new TextFieldParser(ConvertBase64ToText(xmlString));
            using (parser)
            {
                string[] fields = {};
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                string query = "";
                
                int i = 1;

                List<string> columns = getExistingColumns(databaseName,tableName);
                int countColumns = columns.Count() - 2;

                using (SqlConnection conn = new SqlConnection(sqlConnectionString))
                {
                    conn.Open();
                    
                    query = "USE [" + databaseName + "] \n";

                    while (!parser.EndOfData)
                    {
                        fields = parser.ReadFields();
                        if (fields.Count() != countColumns)
                        {
                            errorMessage += i + ",";
                        }
                        else
                        {
                            query += GetInsertQuery(tableName, BatchId, fields);
                        }
                        
                        i++;
                    }

                    SqlCommand myCommand = new SqlCommand(query, conn);
                    try
                    {
                        myCommand.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("\nMessage: Can not insert data" +
                                            "\nException Message: " + ex.Message);
                    }

                }

            }
            resultMessage[0] = BatchId.ToString();
            resultMessage[1] = errorMessage;

            return resultMessage;
        
        }
        
        private Stream ConvertBase64ToText(string xmlString)
        {
            try
            {
                XElement xElem = XElement.Parse(xmlString);

                string base64String = xElem.Element("content").Value;

                byte[] byteArray = Convert.FromBase64String(base64String);

                Stream stream = new MemoryStream(byteArray);

                return stream;
            }
            catch (Exception ex)
            {
                return new MemoryStream();
            }

        }

        private int GetBatchId()
        {
            int batchId = 0;
            
            using (SqlConnection conn = new SqlConnection(sqlConnectionString))
            {
                conn.Open();

                string query = "SELECT NEXT VALUE FOR dbo.csvServiceBrokerSequence;";
                
                SqlCommand myCommand = new SqlCommand(query, conn);
                try
                {
                    batchId = (Int32)myCommand.ExecuteScalar();
                    
                }
                catch (Exception ex)
                {
                    throw new Exception("\nMessage: Can not get BatchId from SQL Sequence" +
                                        "\nException Message: " + ex.Message);
                }

            }

            return batchId;
        }

        private string GetFileNameFromFilePath(string filePath)
        {
            string fileName = Path.GetFileName(filePath).Replace(".csv", "");
            if (String.IsNullOrEmpty(fileName))
            {
                throw new Exception("\nMessage: Cannot get file name\n");
            }
            return fileName;
        }

        private string GetDatabaseNameFromSqlConnectionString(string connectionString)
        {
            string dbName = "";
            string cs = connectionString.Replace(" ", "");
            int start = cs.IndexOf("InitialCatalog=")+15;
            int end = cs.IndexOf(";", start);
            if (start > 0 && end > start)
            {
                dbName = cs.Substring(start, (end - start));
            }
            else
            {
                throw new Exception("\nMessage: Cannot get database name\n");
            }

            if (String.IsNullOrEmpty(dbName))
            {
                throw new Exception("\nMessage: Database name is empty\n");
            }
            return dbName;
        }

        public string getTableName()
        {
            return tableName;
        }

        #endregion

    }
}
