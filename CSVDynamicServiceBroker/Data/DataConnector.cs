﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using SourceCode.SmartObjects.Services.ServiceSDK;
using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;

using CSVDynamicServiceBroker.Interfaces;

namespace CSVDynamicServiceBroker.DataConnectors
{
    /// <summary>
    /// An implementation of IDataConnector responsible for interacting with an underlying system or technology. 
    /// The purpose of this class is to expose and represent the underlying data and services as Service Objects 
    /// which are, in turn, consumed by K2 SmartObjects
    /// TODO: implement the Interface Methods with your own code which interacts with the underlying provider
    /// </summary>
    class DataConnector : IDataConnector
    {
        #region Class Level Fields

        #region Constants
        /// <summary>
        /// Constant for the Type Mappings configuration lookup in the service instance.
        /// </summary>
        private const string TYPEMAPPINGS = "Type Mappings";
        #endregion

        #region Private Fields
        /// <summary>
        /// Local serviceBroker variable.
        /// </summary>
        private ServiceAssemblyBase serviceBroker = null;

        /// <summary>
        /// Sample configuration values for the service instance
        /// defined by the SetupConfiguration() method and set by the GetConfiguration() method
        /// </summary>
        private string connectionString = "Data Source=localhost;Initial Catalog=;Integrated Security=SSPI;";
        private string templateFilePath = "C:\\template.csv";

        private CSVHelper csvHelper;

        #endregion

        #endregion

        #region Constructor
        /// <summary>
        /// Instantiates a new DataConnector.
        /// </summary>
        /// <param name="serviceBroker">The ServiceBroker.</param>
        public DataConnector(ServiceAssemblyBase serviceBroker)
        {
            this.serviceBroker = serviceBroker;
        }
        #endregion

        #region Interface Methods

        #region void SetupConfiguration()
        /// <summary>
        /// Sets up the required configuration parameters for the service instance. 
        /// When a new service instance is registered for this ServiceBroker, the configuration parameters are surfaced to the registration tool. 
        /// The configuration values are provided by the person registering the service instance. 
        /// You can mark the configuration properties as "required" and also provide a default value
        /// </summary>
        public void SetupConfiguration()
        {
            // add configurations for example
            serviceBroker.Service.ServiceConfiguration.Add("SQL Connection String", true, connectionString);
            serviceBroker.Service.ServiceConfiguration.Add("templateFilePath", true, templateFilePath);
        }
        #endregion

        #region void GetConfiguration()
        /// <summary>
        /// Retrieves the configuration from the service instance and stores the retrieved configuration in local variables for later use.
        /// </summary>
        public void GetConfiguration()
        {
            // check authentification mode
            if (serviceBroker.Service.ServiceConfiguration.ServiceAuthentication.AuthenticationMode != AuthenticationMode.ServiceAccount)
            {
                throw new Exception(": You must use a Service Account mode of Authentication!");
            }
            
            // create CSVHelper instance
            csvHelper = new CSVHelper(serviceBroker.Service.ServiceConfiguration);

            // get configurations
            connectionString = serviceBroker.Service.ServiceConfiguration["SQL Connection String"].ToString();
            templateFilePath = serviceBroker.Service.ServiceConfiguration["templateFilePath"].ToString();
            
        }
        #endregion

        #region void SetupService()
        /// <summary>
        /// Sets up the service instance's default name, display name, and description.
        /// </summary>
        public void SetupService()
        {
            CSVHelper Helper = new CSVHelper(serviceBroker.Service.ServiceConfiguration);
            // Create SQL Sequence in database
            Helper.CreateSequence();
            // Generate table in database from csv template
            Helper.Generate();

            string name = "CSV";
            string smoNameAndMethod = Helper.getTableName().ToUpper();
            serviceBroker.Service.Name = name + ".ServiceInstance." + smoNameAndMethod;
            serviceBroker.Service.MetaData.DisplayName = smoNameAndMethod + "Instance";
            serviceBroker.Service.MetaData.Description = " Service Instance for " + smoNameAndMethod + " table";

        }
        #endregion

        #region void DescribeSchema()
        /// <summary>
        /// Describes the schema of the underlying data and services to the K2 platform.
        /// This method is called when a Service Instance is Registered and Refreshed, and 
        /// is responsible for defining the Service Objects that are available in Service Instance.
        /// </summary>
        public void DescribeSchema()
        {
            //TODO: Add code to define one or more ServiceObjects and Add each ServiceObject to the serviceBroker.Service.ServiceObjects collection
            //In a real-world implementation you would connect to the Provider and then run code to discover the Provider's Schema to determine the Objects, their Properties and their Methods.
            //for each Object you would define a ServiceObject with a collection of Property and a collection of Methods

            //OBTAINING THE SECURITY CREDENTIALS FOR A SERVICE INSTANCE AT RUNTIME
            //if you need to obtain the authentication credentials (username/password) for the service instance, query the following properties:
            //Note: password may be blank unless you are using Static or SSO credentials
            //string username = serviceBroker.Service.ServiceConfiguration.ServiceAuthentication.UserName;
            //string password = serviceBroker.Service.ServiceConfiguration.ServiceAuthentication.Password;

            //OBTAINING CONFIGURATION SETTINGS FOR A SERVICE AT RUNTIME
            //if you need to obtain configuration settings for the service instance and you did not define local members, query the following property:
            //string configValue = serviceBroker.Service.ServiceConfiguration["ConfigItemName"].ToString();

            //in this sample, we are calling a sample helper method called GenerateServiceObjects() to define and return two sample service objects
            //refer to the helper method to see how Service Objects are defined, but you will need to implement your own code in a real-world situation
            foreach (ServiceObject svcObj in GenerateServiceObjects())
            {
                serviceBroker.Service.ServiceObjects.Add(svcObj);
            }

        }
        #endregion

        #region void SetTypeMappings()
        /// <summary>
        /// Sets the type mappings used to map the underlying data's types to the equivalent K2 SmartObject types.
        /// </summary>
        public void SetTypeMappings()
        {
            // Variable declaration.
            TypeMappings map = new TypeMappings();

            //TODO: Define Service Type mappings. These mappings are used to "convert" the internal data types 
            //of the Provider's properties to equivalent SmartObject types
            //In the sample code below, we are defining some common type mappings, but yours will probably differ
            map.Add("System.Int16", SoType.Number);
            map.Add("System.Int32", SoType.Number);
            map.Add("System.Int64", SoType.Number);
            map.Add("System.String", SoType.Text);
            map.Add("System.DateTime", SoType.DateTime);
            map.Add("System.Decimal", SoType.Decimal);
            map.Add("System.Guid", SoType.Guid);
            map.Add("System.Uri", SoType.HyperLink);
            map.Add("System.Xml.XmlDocument", SoType.Xml);
            map.Add("System.Boolean", SoType.YesNo);

            // Add the type mappings to the service instance so that they can be retrieved easily
            serviceBroker.Service.ServiceConfiguration.Add(TYPEMAPPINGS, map);
        }
        #endregion

        #region TypeMappings GetTypeMappings()
        /// <summary>
        /// Gets the type mappings used to map the underlying data's types to the appropriate K2 SmartObject types.
        /// </summary>
        /// <returns>A TypeMappings object containing the ServiceBroker's type mappings which were previously stored in the service instance configuration.</returns>
        public TypeMappings GetTypeMappings()
        {
            // Lookup and return the type mappings stored in the service instance.
            return (TypeMappings)serviceBroker.Service.ServiceConfiguration[TYPEMAPPINGS];
        }
        #endregion

        #region void Execute(Property[] inputs, RequiredProperties required, Property[] returns, MethodType methodType, ServiceObject serviceObject)
        /// <summary>
        /// Entry point for all runtime method calls. Executes the Service Object method and returns any data.
        /// This method typically interrogates the requested method and then shells out to a helper method to perform the actual run-time processing
        /// </summary>
        /// <param name="inputProperties">A Property[] array containing all the defined input properties. Properties entered by the user will have a value.
        /// Properties not entered by the user will have a null value</param>
        /// <param name="requiredProperties">A collection of the NAMES of the Required properties for the method (note, not the values!)</param>
        /// <param name="returnProperties">A Property[] array containing all the defined return properties for the requested method</param>
        /// <param name="parameters">A collection of the Parameters defined for the method. Parameters are not included in the return Properties</param>
        /// <param name="methodType">A MethodType indicating what type of Service Object method was requested.</param>
        /// <param name="serviceObject">A ServiceObject definition containing populated properties for use with the method call.</param>
        public void Execute(Property[] inputProperties, RequiredProperties requiredProperties, Property[] returnProperties, MethodParameters parameters, MethodType methodType, ServiceObject serviceObject)
        {
            //TODO: Add runtime execution code here.
            //In a real-world implementation, You will need to interrogate the method type and method name to determine which method was called
            //and then perform the necessary operation in the back-end provider, passing in values from the inputs, required properties and parameters
            //the returnProperties indicates which properties in the serviceObject you should populate. 
            //You must populate the serviceObject with the results from the provider

            //OBTAINING THE SECURITY CREDENTIALS FOR A SERVICE INSTANCE AT RUNTIME
            //if you need to obtain the authentication credentials (username/password) for the service instance, query the following properties:
            //Note: password may be blank unless you are using Static or SSO credentials
            //string username = serviceBroker.Service.ServiceConfiguration.ServiceAuthentication.UserName;
            //string password = serviceBroker.Service.ServiceConfiguration.ServiceAuthentication.Password;

            //OBTAINING CONFIGURATION SETTINGS FOR A SERVICE AT RUNTIME
            //if you need to obtain configuration settings for the service instance and you did not define local members set by the GetConfiguraiotn() method, query the following property:
            //string configValue = serviceBroker.Service.ServiceConfiguration["ConfigItemName"].ToString();

            //in this sample, we are interrogating the method type and calling one of two helper methods, depending on whether this was a "Read" method or a "List" method
            //refer to the helper method to see how the various method parameters are used.
            //You will probably need to handle the other Method Types (e.g. Create, Delete etc.) as well.
            switch (methodType)
            {
                case MethodType.Create:
                    ExecuteRuntimeCreateMethod(inputProperties, requiredProperties, returnProperties, parameters, serviceObject);
                    break;
                default:
                    throw new NotImplementedException("The helper for the specified method type (" + methodType.ToString() + ") has not been implemented");
            }
        }
        #endregion
 
        #region void Dispose()
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            serviceBroker = null;
        }
        #endregion

        #endregion

        #region Helper Methods

        #region ServiceObject[] GenerateServiceObjects()
        /// <summary>
        /// Sample method which generates two Service Objects. You should replace this sample with your own code,
        /// but use the code below as a guide to understand how Service Objects are generated
        /// </summary>
        /// <returns>an Array of ServiceObjects</returns>
        public List<ServiceObject> GenerateServiceObjects()
        {

            List<ServiceObject> sampleSvcObjects = new List<ServiceObject>();

            //variables to hold ServiceObject, Properties and Methods
            ServiceObject obj = null;
            
            //input
            Property fileProperty = null;
            
            //output
            Property batchIdProperty = null;
            Property resultProperty = null;
            Property messageProperty = null;
            
            Method method = null;
            TypeMappings map = GetTypeMappings();

            obj = new ServiceObject();
            obj.Name = this.GetType().Namespace.Replace(" ", "") + ".Object";
            string friendlyName = this.GetType().Assembly.GetName().Name + "." + this.GetType().Name;
            obj.MetaData.DisplayName = friendlyName + " ServiceObject";
            obj.Active = true;

            // Input Property
            fileProperty = new Property();
            fileProperty.Name = "File";
            fileProperty.MetaData.DisplayName = "File";
            fileProperty.Type = "System.String";
            fileProperty.SoType = SoType.File;
            obj.Properties.Add(fileProperty);

            // Output Properties
            batchIdProperty = new Property();
            batchIdProperty.Name = "BatchID";
            batchIdProperty.MetaData.DisplayName = "BatchID";
            batchIdProperty.Type = "System.String";
            batchIdProperty.SoType = map[batchIdProperty.Type];
            obj.Properties.Add(batchIdProperty);

            resultProperty = new Property();
            resultProperty.Name = "Result";
            resultProperty.MetaData.DisplayName = "Result";
            resultProperty.Type = "System.String";
            resultProperty.SoType = map[resultProperty.Type];
            obj.Properties.Add(resultProperty);

            messageProperty = new Property();
            messageProperty.Name = "Message";
            messageProperty.MetaData.DisplayName = "Message";
            messageProperty.Type = "System.String";
            messageProperty.SoType = map[messageProperty.Type];
            obj.Properties.Add(messageProperty);

            // Add Method
            method = new Method();
            method.Name = "Create";
            method.Type = MethodType.Create;
            method.MetaData.DisplayName = "Create" + " Display Name (" + method.Type.ToString() + ")";

            // Set Input Property
            method.InputProperties.Add(fileProperty);
            
            // Set Output Properties
            method.ReturnProperties.Add(batchIdProperty);
            method.ReturnProperties.Add(resultProperty);
            method.ReturnProperties.Add(messageProperty);

            // Mark the key property as required.
            method.Validation.RequiredProperties.Add(fileProperty);

            //add the method to the Service Object
            obj.Methods.Add(method);

            //add the generated service object to the List of ServiceObjects
            sampleSvcObjects.Add(obj);

            //return the collection of defined Service Objects
            return sampleSvcObjects;

        }
        #endregion

        #region Create Method
        private void ExecuteRuntimeCreateMethod(Property[] inputProperties, RequiredProperties requiredProperties, Property[] returnProperties, MethodParameters parameters,  ServiceObject serviceObject)
        {
            //Prepare the Service Object to receive returned data.
            serviceObject.Properties.InitResultTable();

            int BatchID = 0;

            // Get File
            string xmlString = inputProperties[0].Value.ToString();

            try
            {
                string[] result = csvHelper.InsertCSVRows(xmlString);
                BatchID = Int32.Parse(result[0]);
                serviceObject.Properties[1].Value = BatchID;

                if (String.IsNullOrEmpty(result[1]))
                {
                    serviceObject.Properties[2].Value = "success";
                    serviceObject.Properties[3].Value = "";
                }else{
                    serviceObject.Properties[2].Value = "error";
                    serviceObject.Properties[3].Value = "Uploaded file has one or more bad records. Error line numbers: " + result[1];
                }
            }
            catch (Exception ex)
            {
                serviceObject.Properties[1].Value = BatchID;
                serviceObject.Properties[2].Value = "failure";
                serviceObject.Properties[3].Value = "";
            }

            //Commit the changes to the Service Object. 
            serviceObject.Properties.BindPropertiesToResultTable();
        }
        #endregion

        #endregion
    }
}